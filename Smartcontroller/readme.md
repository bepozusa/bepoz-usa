# Bepoz Reports Web API

Bepoz Reports Web API is a .NET library which can be installed as a job in Smart Controller. This plugin extends 
the Bepoz Base Web API by adding endpoints specific to Bepoz Reports.

To install extract .build\release\lib-zip\reports-web-api-{version}.zip to the SmartController installation directory, 
then setup the JobXML_API.dll job in BackOffice (see below)

## JobXML_API.dll job

* Select "Use External Comms"
* Navigate to Bepoz CMP edit appropriate Customer and generate keys for online web apps
** Enter both the MAC Secret Key and ClientID taken from the CMP
* Add a connection Port e.g. 8080
* Select a Till

## Upload to Nuget

* Run the .build\build-upload-nuget-package.cmd script

Both the nuget package and the plugin version will be the same.