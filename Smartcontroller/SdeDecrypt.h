/*                 Copyright (C) 2001 by Mackeral Software                 */

/*-------------------------------------------------------------------------*/
/*                                                                         */
/*                     FILE:   SdeDecrypt.h                                */
/*                     DATE:   25/08/2001                                  */
/*                     AUTHOR: John Trevor                                 */
/*                                                                         */
/*               SdeDecrypt Header File                                    */
/*                                                                         */
/*-------------------------------------------------------------------------*/

/* 25/08/2001 JT
 *            Initial version
 * 3/09/2001  JT
 *            Added extern "C" around function definition.
 * 16/09/2001 JT
 *            Added destsize for max len of destination.
 *            Made which and destsize longs.
 */

//---------------------------------------------------------------------------
//
//      SdeDecrypt - Decrypt a file from a zip file returning a particular
//      text line from the file.
//      Parameters;
//              sdzfilename = Name of SDE encrypted Zip file.
//              filename = Name of file to extract from sdzfilename.
//              which = which line to return;
//                      0 = first line
//                      1 = next available
//              dest = ptr to buffer to receive line. must be large enough
//                     to receive largest expected line.
//              destsize = maximum size of line (includes null).
//      Return values:
//              > 0     length of line returned.
//               0      means no more lines.
//              -1      File not found.
//              -3      Internal error.
//              -5      Invalid which value.
//              -6      Not Registered.
//              -7      Zip File not found.
//
extern "C" int __declspec( dllexport ) __stdcall SdeDecrypt(char *sdzfilename, char *filename, long which, char *dest, long destsize);

