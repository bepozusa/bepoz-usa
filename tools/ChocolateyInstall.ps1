try {
    $bepoztempdir = "C:\bepoz\temp\"
    copy-item (Join-Path "$(Get-PackageRoot($MyInvocation))\version" '*') -Force -Recurse $bepoztempdir
    Write-ChocolateySuccess 'BepozVersion'
} catch {
  Write-ChocolateyFailure 'BepozVersion' $($_.Exception.Message)
  throw
}