### Version ###
4.5.2.163
### What's New ###
* ANZ BladePAY w/ Surefire Pay At Table App “BETA”
* API Support For Gift Certificates
* Electronic Invoice Receiving Store Selection
* Maxetag Mifare Reader
* MyEG Interface
* New Raffle Operator Privileges
* PO Header Support Multiple Taxes
* Product Inactive By Store
### Overview ###
* The 4.5.2 release includes enhancements to Operator privileges, helpful new interfaces, upgrades to receiving electronic invoices, and more...!
* QR Codes can now be added to receipts for greater marketing presence
* Checkout the attached pages for more on the latest improvements, features and functions